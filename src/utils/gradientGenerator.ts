import React from "react";
import chroma from "chroma-js";

export const gradientGenerator = (
  hexColor: string,
  degrees: number = 180
): React.CSSProperties => {
  const mainColor = chroma(hexColor);
  const secondaryColor = chroma(hexColor).brighten(2).saturate(4);

  const background = `linear-gradient(${degrees}deg, ${mainColor.css()} 0%, ${secondaryColor.css()} 100%)`;

  return {
    background,
  };
};
