import { Item } from "../interfaces/items";
import { validateNotNil, validateString, validateWithKey } from "./asserts";

export function validateItem(item: unknown): asserts item is Item {
  validateNotNil(item);
  validateWithKey("id", item);
  validateWithKey("name", item);
  validateWithKey("hexColor", item);

  validateString(item.id);
  validateString(item.hexColor);
  validateString(item.name);
}
