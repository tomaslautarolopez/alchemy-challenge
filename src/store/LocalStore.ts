import { ItemId, ItemValidator, Store, StoreResult } from "./store";

const parseNumber = (numberString: string | null): number => {
  return Number.isNaN(Number(numberString)) ? 0 : Number(numberString);
};

const COUNTER_KEY = "counter";

export class LocalStore<T> implements Store<T> {
  itemValidator: ItemValidator<T>;

  constructor(itemValidator: ItemValidator<T>) {
    this.itemValidator = itemValidator;
  }

  setItem(item: T) {
    this.itemValidator(item);

    const itemId = parseNumber(window.localStorage.getItem(COUNTER_KEY)) + 1;
    window.localStorage.setItem(itemId.toString(), JSON.stringify(item));

    window.localStorage.setItem(COUNTER_KEY, JSON.stringify(itemId));

    return {
      item,
      itemId,
    };
  }

  getItem(itemId: ItemId) {
    const item: any = JSON.parse(
      window.localStorage.getItem(itemId.toString()) ?? ""
    );

    this.itemValidator(item);

    return {
      item: item,
      itemId,
    };
  }

  deleteItem(itemId: ItemId) {
    window.localStorage.removeItem(itemId.toString());
  }

  updateItem(itemId: ItemId, updateItem: T) {
    this.itemValidator(updateItem);

    window.localStorage.setItem(itemId.toString(), JSON.stringify(updateItem));

    return {
      item: updateItem,
      itemId,
    };
  }

  getAllItems() {
    const counter = parseNumber(window.localStorage.getItem(COUNTER_KEY));
    const allItems: StoreResult<T>[] = [];

    for (let index = 1; index <= counter; index++) {
      try {
        allItems.push(this.getItem(index));
      } catch {}
    }

    return allItems;
  }
}
