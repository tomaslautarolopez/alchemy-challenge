export type ItemId = number;
export type ItemValidator<T> = (item: unknown) => asserts item is T;

export interface StoreResult<T> {
  itemId: ItemId;
  item: T;
}

export interface Store<T> {
  setItem: (item: T) => StoreResult<T>;
  getItem: (itemId: ItemId) => StoreResult<T>;
  getAllItems: () => StoreResult<T>[];
  deleteItem: (itemId: ItemId) => void;
  updateItem: (itemId: ItemId, item: T) => StoreResult<T>;
}
