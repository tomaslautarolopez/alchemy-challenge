export enum ItemTypes {
  COMPONENT = "COMPONENT",
  ELEMENT = "ELEMENT",
}

export interface Item {
  id: string;
  name: string;
  hexColor: string;
}
