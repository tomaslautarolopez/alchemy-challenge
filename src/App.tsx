import React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import { Canvas } from "./components/Canvas/Canvas";
import { SideBar } from "./components/SideBar/SideBar";
import { Item } from "./interfaces/items";

import "./App.css";

const items: Item[] = [
  { id: "1", name: "Item 1", hexColor: "#ed5d53" },
  { id: "2", name: "Item 2", hexColor: "#53edd1" },
  { id: "3", name: "Item 3", hexColor: "#f089d2" },
];

const SIDEBAR_PX_WIDTH = 300;

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <div className="app">
        <div className="app__side-bar">
          <SideBar sideBarPxWidth={SIDEBAR_PX_WIDTH} items={items} />
        </div>
        <div className="app__canvas">
          <Canvas sideBarPxWidth={SIDEBAR_PX_WIDTH} />
        </div>
      </div>
    </DndProvider>
  );
}

export default App;
