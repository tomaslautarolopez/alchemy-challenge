import { useEffect, useRef } from "react";

export function useClickOutside(callback?: Function) {
  const ref = useRef<HTMLElement | null>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        ref.current !== null &&
        event.target !== null &&
        !ref.current.contains(event.target as Node)
      ) {
        callback?.();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [callback]);

  return ref;
}
