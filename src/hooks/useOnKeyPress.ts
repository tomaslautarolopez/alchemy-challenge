import { useEffect } from "react";

export function useOnKeyPress(
  callback: (this: Document, ev: KeyboardEvent) => any
) {
  useEffect(() => {
    document.addEventListener("keydown", callback);

    return () => {
      document.removeEventListener("keydown", callback);
    };
  }, [callback]);
}
