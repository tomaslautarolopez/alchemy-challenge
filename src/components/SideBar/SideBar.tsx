import React from "react";
import { Item } from "../../interfaces/items";
import { Component } from "../Component/Component";

import "./styles.css";

interface Props {
  items: Item[];
  sideBarPxWidth: number;
}

export const SideBar: React.VFC<Props> = ({ items, sideBarPxWidth }) => {
  return (
    <div
      className="side-bar"
      style={{
        width: sideBarPxWidth,
      }}
    >
      <ul className="side-bar__list">
        {items.map((item) => (
          <li key={item.id}>
            <Component item={item} />
          </li>
        ))}
      </ul>
    </div>
  );
};
