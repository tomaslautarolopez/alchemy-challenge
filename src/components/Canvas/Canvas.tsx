import React, { useCallback, useState } from "react";
import { useDrop } from "react-dnd";
import {
  validateNotNil,
  validateNumber,
  validateWithKey,
} from "../../utils/asserts";
import { LocalStore } from "../../store/LocalStore";
import { ItemId, StoreResult } from "../../store/store";
import { useOnKeyPress } from "../../hooks/useOnKeyPress";
import { Item, ItemTypes } from "../../interfaces/items";
import { validateItem } from "../../utils/validateItem";
import { Element } from "../Element/Element";

import "./styles.css";

interface Props {
  sideBarPxWidth: number;
}

interface MousePosition {
  x: number;
  y: number;
}

export interface DroppedElement {
  item: Item;
  position: MousePosition;
}

export function validateDroppedElement(
  droppedElement: unknown
): asserts droppedElement is DroppedElement {
  validateNotNil(droppedElement);
  validateWithKey("item", droppedElement);
  validateWithKey("position", droppedElement);
  validateItem(droppedElement.item);
  validateWithKey("x", droppedElement.position);
  validateWithKey("y", droppedElement.position);
  validateNumber(droppedElement.position.x);
  validateNumber(droppedElement.position.y);
}

const localItemStore = new LocalStore<DroppedElement>(validateDroppedElement);

const useCanvas = (sideBarPxWidth: number) => {
  const [selectedItemId, setSelectedItemId] = useState<ItemId | undefined>();
  const [droppedElements, setDroppedElements] = useState(
    localItemStore.getAllItems()
  );

  const onClickOutsideItem = useCallback(() => {
    setSelectedItemId(undefined);
  }, []);

  useOnKeyPress(
    useCallback(
      (event: KeyboardEvent) => {
        if (
          selectedItemId !== undefined &&
          (event.code === "Delete" || event.code === "Backspace")
        ) {
          localItemStore.deleteItem(selectedItemId);
          setSelectedItemId(undefined);
          setDroppedElements((currentDroppedElements) =>
            currentDroppedElements.filter(
              (droppedElement) => droppedElement.itemId !== selectedItemId
            )
          );
        }
      },
      [selectedItemId]
    )
  );

  const onClickItem = (droppedElement: StoreResult<DroppedElement>) => {
    if (selectedItemId === droppedElement.itemId) {
      setSelectedItemId(undefined);
    } else {
      setSelectedItemId(droppedElement.itemId);
    }
  };

  const [, dropComponent] = useDrop(() => ({
    accept: ItemTypes.COMPONENT,
    drop: (item: Item, monitor) => {
      const position = monitor.getClientOffset();

      if (position) {
        setDroppedElements((currentDroppedElements) => {
          const storedItem = localItemStore.setItem({
            item,
            position: {
              x: position.x - sideBarPxWidth,
              y: position.y,
            },
          });

          return [...currentDroppedElements, storedItem];
        });
      }
    },
  }));

  const [, dropElement] = useDrop(() => ({
    accept: ItemTypes.ELEMENT,
    drop: (element: StoreResult<DroppedElement>, monitor) => {
      const position = monitor.getClientOffset();

      if (position) {
        setDroppedElements((currentDroppedElements) => {
          const updatedItem = localItemStore.updateItem(element.itemId, {
            item: element.item.item,
            position: {
              x: position.x - sideBarPxWidth,
              y: position.y,
            },
          });

          return [
            ...currentDroppedElements.filter(
              (droppedElement) => droppedElement.itemId !== element.itemId
            ),
            updatedItem,
          ];
        });
      }
    },
  }));

  return {
    onClickItem,
    onClickOutsideItem,
    droppedElements,
    selectedItemId,
    dropComponent,
    dropElement,
  };
};

export const Canvas: React.VFC<Props> = ({ sideBarPxWidth }) => {
  const {
    onClickOutsideItem,
    onClickItem,
    droppedElements,
    selectedItemId,
    dropComponent,
    dropElement,
  } = useCanvas(sideBarPxWidth);
  // const canvasRef = useRef<HTMLCanvasElement>(null);

  // useEffect(() => {
  //   const handleResize = () => {
  //     if (canvasRef.current) {
  //       canvasRef.current.width = window.innerWidth - sideBarPxWidth;
  //       canvasRef.current.height = window.innerHeight;
  //     }
  //   };

  //   handleResize();
  //   window.addEventListener("resize", handleResize);

  //   return () => {
  //     window.removeEventListener("resize", handleResize);
  //   };
  // }, [sideBarPxWidth, canvasRef]);

  return (
    <div
      ref={(ref) => {
        dropElement(ref);
        dropComponent(ref);
      }}
      className="canvas"
    >
      {/* <canvas ref={canvasRef} /> */}
      {droppedElements.map((droppedElement) => (
        <Element
          key={droppedElement.itemId}
          selectedItemId={selectedItemId}
          element={droppedElement}
          onClick={() => onClickItem(droppedElement)}
          onClickOutside={
            droppedElement.itemId === selectedItemId
              ? onClickOutsideItem
              : undefined
          }
        />
      ))}
    </div>
  );
};
