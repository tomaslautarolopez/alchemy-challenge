import { useDrag } from "react-dnd";
import classnames from "classnames";

import { ItemTypes } from "../../interfaces/items";
import { DroppedElement } from "../Canvas/Canvas";
import { Component } from "../Component/Component";
import { ItemId, StoreResult } from "../../store/store";

import "./styles.css";

interface Props {
  element: StoreResult<DroppedElement>;
  onClickOutside?: Function;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  selectedItemId?: ItemId;
}

export const Element: React.VFC<Props> = ({
  element,
  onClickOutside,
  onClick,
  selectedItemId,
}) => {
  const [{ opacity }, dragRef] = useDrag(
    () => ({
      type: ItemTypes.ELEMENT,
      item: element,
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.5 : 1,
      }),
    }),
    []
  );

  return (
    <div
      key={element.itemId}
      ref={dragRef}
      className={classnames({
        element: true,
        selected: element.itemId === selectedItemId,
      })}
      style={{
        top: element.item.position.y,
        left: element.item.position.x,
        opacity,
      }}
    >
      <Component
        disableDrag
        onClickOutside={onClickOutside}
        onClick={onClick}
        item={element.item.item}
      />
    </div>
  );
};
