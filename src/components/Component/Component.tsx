import { MouseEventHandler } from "react";
import { gradientGenerator } from "../../utils/gradientGenerator";
import { useDrag } from "react-dnd";
import { useClickOutside } from "../../hooks/useClickOutside";
import { Item, ItemTypes } from "../../interfaces/items";

import "./styles.css";

interface Props {
  item: Item;
  onClickOutside?: Function;
  onClick?: MouseEventHandler<HTMLDivElement>;
  disableDrag?: boolean;
}

export const Component: React.VFC<Props> = ({
  item,
  onClickOutside,
  onClick,
  disableDrag,
}) => {
  const outsideRef = useClickOutside(onClickOutside);
  const [{ opacity }, dragRef] = useDrag(
    () => ({
      type: ItemTypes.COMPONENT,
      item,
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.5 : 1,
      }),
      canDrag: !disableDrag,
    }),
    []
  );

  return (
    <div
      onClick={onClick}
      ref={(ref) => {
        outsideRef.current = ref;
        dragRef(ref);
      }}
      className="item"
      style={{ ...gradientGenerator(item.hexColor), opacity }}
    />
  );
};
